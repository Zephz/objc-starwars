//
//  ViewController.h
//  StarWarsObjC
//
//  Created by Mac on 3/8/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;
@import FirebaseDatabase;

@interface SWView : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSString* url;
@property (strong, nonatomic) FIRDatabaseReference *ref;


@property (strong, nonatomic) IBOutlet UISegmentedControl *segCtrl;





@end



