//
//  ViewController.m
//  StarWarsObjC
//
//  Created by Mac on 3/8/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//


#import "SWView.h"
#import "SWDetails.h"
#import "ViewCell.h"
#import "NetworkingServices.h"

@import Firebase;
@import FirebaseDatabase;


/*
 Issues:
 
 Whenever the favorites has nothing in it, if you go to the favorites tab and try to
 favorite multiple things, it will crash
 
 * Try to fix tomorrow, somehow make it load an empty table view?
 
 *delete from array aswell
 
 
 *add images, wont be to hard, might not do just cause
 
 *add otheres, will make files really big unless I split it, not sure if I can do dependency injection in obj c
 
 
 */



@interface SWView()

@end




@implementation SWView{
    NSArray *tableInfo;
    
    __weak IBOutlet UITableView *tableView;
    
    SWChar *info;
    
    
    
    NSArray *CharNames;
    NSArray *CharUrl;
    
    NSMutableArray *FavCharNames;
    NSMutableArray *FavUrl;
    
    CGRect *temp;
    
    NSString *next;
    
    
    
}

@synthesize ref;
@synthesize segCtrl;

- (IBAction)segCtrl:(UISegmentedControl *)sender {
    
    //basically just recalls to update favorites whenever user switches to
    switch (segCtrl.selectedSegmentIndex) {
        case 0:
            [tableView reloadData];
            break;
        case 1:
            [self getFavorites];
            break;
        default:
            break;
    }
}




- (IBAction)FavClick:(UIButton *)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tableView];
    NSIndexPath *indexPath = [tableView indexPathForRowAtPoint:buttonPosition];
    
    switch (segCtrl.selectedSegmentIndex) {
            
        case 0: //Adding stuff
            [[[[ref child:@"Favorites"] child:CharNames[indexPath.row]] child:@"url"] setValue:CharUrl[indexPath.row]];
            break;
            
            
        case 1: //Deleting stuff
            if ([[FavCharNames firstObject] count] != 0) {
                [[[ref child:@"Favorites"]  child:[FavCharNames firstObject][indexPath.row]] removeValue];
               
                [[FavCharNames firstObject] removeObjectAtIndex:indexPath.row];
                [self getFavorites];
            }
            break;
        default:
            break;
    }
    
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.url = @"https://swapi.co/api/people/"; //Just doing people for now
    self.ref = [[FIRDatabase database] reference];
    
    [self getFavorites];
    
    
    CharUrl = [[NSMutableArray alloc] init];
    CharNames = [[NSMutableArray alloc] init];
    FavCharNames = [[NSMutableArray alloc] init];
    FavUrl = [[NSMutableArray alloc] init];
    
    [self callNetwork:_url];
    
}


- (void)callNetwork: (NSString*)url{
    
    [NetworkingServices getObjectFromNetwork:url :^SWChar *(id obj, NSError *error) {
        
        
        if (error != nil){
            NSLog(@"%@", [error localizedDescription]);
            return nil;
        }
        
        info = obj;
        
        next = info.next;
        
        CharUrl = [CharUrl arrayByAddingObjectsFromArray:info.charUrl];
        CharNames = [CharNames arrayByAddingObjectsFromArray:info.charName];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [tableView reloadData];
        });
        
        
        //return what?
        return nil;
    }];
    
}

- (void) getFavorites{
    //grab favorites from firebase and store them in fav array
    ref = [[FIRDatabase database] reference];
    [ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *usersDict = snapshot.value;
        
        if (usersDict != (id)[NSNull null]){
            [FavCharNames removeAllObjects];
            [FavUrl removeAllObjects];
            
            
            //Casts what I get back as a mutablearray
            NSMutableArray *boop = [[NSMutableArray alloc] init];
            boop = [[[usersDict valueForKey:@"Favorites"] allKeys] mutableCopy];
            [FavCharNames addObject:boop];
            
            
            for (int x = 0; x < [[FavCharNames firstObject] count]; x++) {
                [FavUrl addObject:[[[usersDict objectForKey:@"Favorites"] objectForKey:[FavCharNames firstObject][x]] valueForKey:@"url"]];
            }
            
            
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [tableView reloadData];
        });
    }withCancelBlock:^(NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
    }];
    
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch ([segCtrl selectedSegmentIndex]) {
        case 0:
            return [CharNames count];
            break;
        case 1:
            return [[FavCharNames firstObject] count];
            break;
        default:
            return 0;
            break;
    }
    
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    ViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    
    if (cell == nil) {
        cell = [[ViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    switch ([segCtrl selectedSegmentIndex]) {
        case 0:
            //As long as next isn't null and the user scrolls to the bottom of the screen then it will
            //call the network and grab more data to display
            if (next != (id)[NSNull null] && indexPath.row == CharNames.count - 1){
                [self callNetwork:next];
            }
            
            cell.nameLabel.text = [CharNames objectAtIndex:indexPath.row];
            break;
        case 1:
            //This grabs stuff out of favorites to display
            cell.nameLabel.text = [[FavCharNames firstObject] objectAtIndex:indexPath.row];
            
            break;
        default:
            break;
    }
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"toDetails" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"toDetails"])
    {
        NSIndexPath *indexPath = [tableView indexPathForSelectedRow];
        
        SWDetails *vc = [segue destinationViewController];
        
        switch (segCtrl.selectedSegmentIndex) {
            case 0:
                vc.url = [CharUrl objectAtIndex:indexPath.row];
                break;
            case 1:
                vc.url = [FavUrl objectAtIndex:indexPath.row];
                break;
            default:
                break;
        }
        
    }
    
}




@end



