//
//  UIViewController+LogIn.m
//  StarWarsObjC
//
//  Created by Mac on 3/12/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//

#import "LogInController.h"
#import "SWView.h"
@import Firebase;

@implementation LogInController

@synthesize userText;
@synthesize passText;
@synthesize loginButton;


- (IBAction)loginButton:(UIButton *)sender {
    
    if ([userText.text  isEqual: @""] || [passText.text  isEqual: @""]){
        
        if ([userText.text  isEqual: @""]) {
            NSLog(@"Enter a username");
        }else{
            NSLog(@"Enter a password");
        }
        
    } else {
        
        [[FIRAuth auth] signInWithEmail:userText.text password:passText.text completion:^(FIRUser *_Nullable user, NSError *_Nullable error){
            
            if (error == nil){
                
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"HomeNav"];
                vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self presentViewController:vc animated:YES completion:NULL];
                
    
            } else{
                NSLog(@"Enter wrong username or pass");
                userText.text = @"";
                passText.text = @"";
                
                
                [userText becomeFirstResponder];
            }
            
            
            
            
        }];
        
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    passText.secureTextEntry = YES;
}


@end
