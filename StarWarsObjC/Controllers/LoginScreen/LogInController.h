//
//  UIViewController+LogIn.h
//  StarWarsObjC
//
//  Created by Mac on 3/12/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogInController: UIViewController

@property (strong, nonatomic) IBOutlet UITextField *userText;
@property (strong, nonatomic) IBOutlet UITextField *passText;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;

@end
