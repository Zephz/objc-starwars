//
//  UIViewController+SWDetails.h
//  StarWarsObjC
//
//  Created by Mac on 3/9/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWDetails : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSString *url;


@property NSString *name;
@property NSString *birth_year;
@property NSString *eye_color;
@property NSString *skin_color;
@property NSString *gender;
@property NSString *hair_color;
@property NSString *height;
@property NSString *homeworld;
@property NSString *mass;

@property NSMutableArray *films;
@property NSMutableArray *species;
@property NSMutableArray *starships;
@property NSMutableArray *vehicles;

//birth_year
//eye_color
//films []
//gender
//hair_color
//height
//homeworld
//mass
//name
//skin_color
//species []
//starships []
//vehicles []


@end
