//
//  UIViewController+SWDetails.m
//  StarWarsObjC
//
//  Created by Mac on 3/9/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//

#import "SWDetails.h"

@interface SWDetails()



@end

@implementation SWDetails{
    
    __weak IBOutlet UITableView *tableView;
    
    NSArray *CharTitles;
    NSString *name;
    NSString *birth_year;
    NSString *eye_color;
    NSString *skin_color;
    NSString *gender;
    NSString *hair_color;
    NSString *height;
    NSString *homeworld;
    NSString *mass;
    
    NSMutableArray *films;
    NSMutableArray *species;
    NSMutableArray *starships;
    NSMutableArray *vehicles;
}

@synthesize url, name, films, birth_year, eye_color, gender, hair_color, height, homeworld, mass, skin_color, starships, vehicles, species;




- (void)viewDidLoad {
    [super viewDidLoad];


    [self setUp]; //This function just sets everything up instead of it all being in this function



    [self networkCall:url];
}

-(void) setUp{
    
    films = [[NSMutableArray alloc] init];
    species = [[NSMutableArray alloc] init];
    starships = [[NSMutableArray alloc] init];
    vehicles = [[NSMutableArray alloc] init];
    
    CharTitles = [NSArray arrayWithObjects:@"Name", @"Birth Year", @"Eye Color", @"Gender", @"Hair Color", @"Height", @"Homeworld", @"Mass", @"Skin Color", @"Films", @"Starships", @"Species", @"Vehicles", nil];
}


- (void) networkCall: (NSString*)url{
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:url] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        // NSLog(@"%@", json);
        
        name = json[@"name"];
        birth_year = json[@"birth_year"];
        eye_color = json[@"eye_color"];
        gender = json[@"gender"];
        hair_color = json[@"hair_color"];
        height = json[@"height"];
        homeworld = json[@"homeworld"];
        mass = json[@"mass"];
        skin_color = json[@"skin_color"];
        
        NSArray* jsonFilm = json[@"films"];
        NSArray* jsonSpecies = json[@"species"];
        NSArray* jsonStarships = json[@"starships"];
        NSArray* jsonVehicles = json[@"vehicles"];
        
        
        for (int x = 0; x < jsonFilm.count; x++){
            [films addObject:jsonFilm[x]];
        }
        for (int x = 0; x < jsonSpecies.count; x++){
            [species addObject:jsonSpecies[x]];
        }
        for (int x = 0; x < jsonStarships.count; x++){
            [starships addObject:jsonStarships[x]];
        }
        for (int x = 0; x < jsonVehicles.count; x++){
            [vehicles addObject:jsonVehicles[x]];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [tableView reloadData];
        });
    }];
    [dataTask resume];
    
    
    
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return CharTitles[section];
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    
    switch (indexPath.section) {
        case 0:
            cell.textLabel.text = name;
            break;
        case 1:
            cell.textLabel.text = birth_year;
            break;
        case 2:
            cell.textLabel.text = eye_color;
            break;
        case 3:
            cell.textLabel.text = gender;
            break;
        case 4:
            cell.textLabel.text = hair_color;
            break;
        case 5:
            cell.textLabel.text = height;
            break;
        case 6:
            cell.textLabel.text = homeworld;
            break;
        case 7:
            cell.textLabel.text = mass;
            break;
        case 8:
            cell.textLabel.text = skin_color;
            break;
        case 9:
            cell.textLabel.text = films[indexPath.row];
            break;
        case 10:
            cell.textLabel.text = starships[indexPath.row];
            break;
        case 11:
            cell.textLabel.text = species[indexPath.row];
            break;
        case 12:
            cell.textLabel.text = vehicles[indexPath.row];
            break;
        default:
            break;
    }
    
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 13;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 9:
            return films.count;
            break;
        case 10:
            return starships.count;
            break;
        case 11:
            return species.count;
            break;
        case 12:
            return vehicles.count;
            break;
        default:
            return 1;
            break;
    }
    
}



//if I implement other stuff, not atm

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    switch (indexPath.section) {
//        case 6://Homeworld
//            if (homeworld != (id)[NSNull null]){
//                url = homeworld;
//            }
//            [self networkCall:url];
//            break;
//        case 9://Films
//            NSLog(@"%@", films[indexPath.row]);
//            url = films[indexPath.row];
//            [self networkCall:url];
//            break;
//        case 10://Starships
//            url = starships[indexPath.row];
//            [self networkCall:url];
//            break;
//        case 11://Species
//            url = species[indexPath.row];
//            [self networkCall:url];
//            break;
//        case 12://Vehicles
//            url = vehicles[indexPath.row];
//            [self networkCall:url];
//            break;
//        default:
//            break;
//    }
//
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//}

@end




