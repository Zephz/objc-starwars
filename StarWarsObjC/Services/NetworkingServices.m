//
//  NSObject+NetworkingServices.m
//  StarWarsObjC
//
//  Created by Mac on 3/14/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//

#import "NetworkingServices.h"



@implementation  NetworkingServices: NSObject


+ (void)getObjectFromNetwork:(NSString*)url : (SWChar* (^)(id obj, NSError*))blocky{
    
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:url] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error != nil){
            blocky(nil, error);
            return;
        }
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

        
        //  NSLog(@"%@", json); //Check the entire json
        
        @try {
            SWChar * object = [[SWChar alloc] init];
            
            NSArray<NSDictionary *> *results = json[@"results"];

            NSString *url = json[@"next"];
            object.next = url;
            
            NSMutableArray *tempName;
            NSMutableArray *tempUrl;
            
            tempName = [[NSMutableArray alloc] init];
            tempUrl = [[NSMutableArray alloc] init];
            
            for(NSDictionary * dict in results){
                [tempName addObject:dict[@"name"]];
                [tempUrl addObject:dict[@"url"]];
            }
            object.charName = tempName;
            object.charUrl = tempUrl;
            
            blocky(object, nil);
            
        } @catch (NSException *exception) {
            NSLog(@"%@", exception);
            blocky(nil, error);
        }

    }];

    [dataTask resume];
    
}

@end
