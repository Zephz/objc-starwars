//
//  NSObject+NetworkingServices.h
//  StarWarsObjC
//
//  Created by Mac on 3/14/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWChar.h"

@interface NetworkingServices: NSObject

+ (void)getObjectFromNetwork:(NSString*) url : (SWChar* (^)(id obj, NSError*))blocky;


@end
