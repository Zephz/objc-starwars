//
//  UITableViewCell+ViewCell.h
//  StarWarsObjC
//
//  Created by Mac on 3/12/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UIButton *nameImage;

@end
