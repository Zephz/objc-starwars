//
//  NSObject+Objects.h
//  StarWarsObjC
//
//  Created by Mac on 3/15/18.
//  Copyright © 2018 Ghost World. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWChar: NSObject

@property NSString *next;
@property NSArray *charName;
@property NSArray *charUrl;


@end


